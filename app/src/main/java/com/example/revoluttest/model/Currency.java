package com.example.revoluttest.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.revoluttest.repository.Converters;

import java.util.Map;

@Entity(tableName = "currency")
public class Currency {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name ="code")
    private String code;
    @ColumnInfo(name ="title")
    private String title;

    @ColumnInfo(name = "rates")
    @TypeConverters({Converters.class})
    private Map<String, Double> rates;

    @ColumnInfo(name = "conversion_rate")
    private double conversionRate;





    public Currency(String code, Map<String, Double> rates) {
        this.code = code;
        this.rates = rates;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(double conversionRate) {
        this.conversionRate = conversionRate;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", rates=" + rates +
                '}';
    }
}
