package com.example.revoluttest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CurrencyResponse {
    @SerializedName("base")
    @Expose
    private String code;
//    private String title;
    @SerializedName("rates")
    @Expose
    private Map<String, Double> rates;

    public CurrencyResponse(String code, Map<String, Double> rates) {
        this.code = code;
        this.rates = rates;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "CurrencyResponse{" +
                "code='" + code + '\'' +
                ", rates=" + rates +
                '}';
    }
}
