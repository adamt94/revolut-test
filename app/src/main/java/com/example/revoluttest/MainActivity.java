package com.example.revoluttest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.revoluttest.model.Currency;
import com.example.revoluttest.modelview.CurrencyModelView;
import com.example.revoluttest.util.Resource;
import com.example.revoluttest.util.TextWatcherListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TextWatcherListener {

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdaptor;
    private List<Currency> mCurrencies;
    private CurrencyModelView mCurrencyModelView;
    private Integer  currentPosition = null;


    private static final String TAG = "MainActivity";
    String[] baseCurrencies = {"AUD","GBP","BGN","BRL","CAD","CHF","CNY","EUR","USD",};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRecylerView();
        mCurrencyModelView = ViewModelProviders.of(this).get(CurrencyModelView.class);
        subscribeObservers();
        mCurrencies = new ArrayList<>();
        for(String value: baseCurrencies) {
            mCurrencyModelView.getCurrencyApi(value);
        }


    }


    private void initRecylerView() {
        mRecyclerView = findViewById(R.id.recycler);
        mRecyclerAdaptor = new RecyclerAdapter(this);
        mRecyclerView.setAdapter(mRecyclerAdaptor);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private void subscribeObservers(){
        mCurrencyModelView.getCurrency().observe(this, new Observer<Resource<List<Currency>>>() {
            @Override
            public void onChanged(Resource<List<Currency>> currencyResource) {
                if(currencyResource.data != null) {
                    mCurrencies = currencyResource.data;
                    if(currentPosition != null) {
                        //update all the conversion values apart from currently selected
                        List<Currency> updatedRates =  calculateRates(mCurrencies, mCurrencies.get(currentPosition).getCode(), mCurrencies.get(currentPosition).getConversionRate());
                        mRecyclerAdaptor.setCurrency(updatedRates,currentPosition);
                    }else {
                        mRecyclerAdaptor.setCurrency(currencyResource.data);
                    }
                }



            }
        });
    }

    @Override
    public void afterTextChanged(View v, Editable s) {

    }

    @Override
    public void onTextChanged(View v, CharSequence s, int start, int before, int count,int position) {

        if(v.hasFocus()) {
            TextView tv = v.findViewById(R.id.currency_title);
            try {
                double value = Double.parseDouble(s.toString());
                String base = tv.getText().toString();
                Currency currentRates = mCurrencies.get(0);

                    mCurrencyModelView.setCurrencyRates(calculateRates(mCurrencies,base,value));
                    currentPosition =position;
                  //  mRecyclerAdaptor.setCurrency(mCurrencies, position);


            }catch(NumberFormatException e){

            }

        }


    }
    public List<Currency> calculateRates(List<Currency> currencies, String base, double value){
        if(value > 0) {
            Currency currentRates = currencies.get(0);
            for (Currency currency : currencies) {

                if (currency.getCode().equals(base)) {
                    currentRates = currency;
                    Log.d(TAG, "onTextChanged: " + currency);
                    break;
                }
            }
            Log.d(TAG, "onTextChangedRates: " + currentRates.getRates().get("EUR"));
            //calculate conversions
            for (Currency currency : currencies) {
                if (!currency.getCode().equals(base)) {
                    if (currentRates.getRates() != null) {
                        double total = (value * currentRates.getRates().get(currency.getCode()));
                        currency.setConversionRate(Math.round(total * 100.0) / 100.0);

                    }
                } else {
                    currency.setConversionRate(value);
                }
            }




            return currencies;

        }
        return null;

    }

    @Override
    public void beforeTextChanged(View v, CharSequence s, int start, int count, int after) {

    }
}
