package com.example.revoluttest.repository;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.example.revoluttest.requests.ApiResponse;
import com.example.revoluttest.AppExecutors;
import com.example.revoluttest.requests.ServiceGenerator;
import com.example.revoluttest.model.Currency;
import com.example.revoluttest.model.CurrencyResponse;
import com.example.revoluttest.persistance.CurrencyDao;
import com.example.revoluttest.util.NetworkBoundResource;
import com.example.revoluttest.util.Resource;

import java.util.List;


public class Repository {
    private static Repository instance;
    private static CurrencyDao currencyDao;
    private static final String TAG = "CoinRepository";



    public static Repository getInstance(Context context){
        if(instance == null){
            instance = new Repository(context);
        }
        return instance;
    }

    public Repository(Context context){
        currencyDao = Database.getInstance(context).getDao();
    }

    public static void updateCurrencies(final List<Currency> currencies){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                for(Currency currency: currencies) {
                    currencyDao.updateCurrency(currency);
                }
            }
        });

    }
    public static LiveData<Resource<List<Currency>>> getConversion(final String base){
        return new NetworkBoundResource<List<Currency>, CurrencyResponse>(AppExecutors.getInstance()) {

            @Override
            protected void saveCallResult(@NonNull CurrencyResponse item) {
                if(item != null) {

                    Currency currency = new Currency(item.getCode(),item.getRates());
                    currency.setTitle(getCurrencyFullName(item.getCode()));
                    Log.d(TAG, "saveCallResult: "+ item);
                    for(long rowid: currencyDao.insertCurrency(currency)){
                        if(rowid == -1){
                            currencyDao.updateCurrencyItem(currency.getCode(),currency.getRates());
                        }
                    }
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<Currency> data) {
                return true;
            }

            @Override
            protected LiveData<List<Currency>> loadFromDb() {
                return currencyDao.getCurrency();
            }

            @Override
            protected LiveData<ApiResponse<CurrencyResponse>> createCall() {
                Log.d(TAG, "createCall: "+base);
                return ServiceGenerator.getApi().getCurrency( base);
            }
        }.getAsLiveData();


    }

    public static String getCurrencyFullName(String value){
        String name = "";
        switch (value){
            case "EUR":
                name = "Euro";
                break;
            case "GBP":
                name = "Pound";
                break;
            case "CAD":
                name = "Canadian Dollar";
                break;
            case "SEK":
                name = "Swedish Krona";
                break;
            case "USD":
                name = "US Dollar";

        }

        return name;
    }


}
