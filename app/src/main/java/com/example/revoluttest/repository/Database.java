package com.example.revoluttest.repository;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.revoluttest.model.Currency;
import com.example.revoluttest.persistance.CurrencyDao;


@androidx.room.Database(entities = {Currency.class}, version = 1)
@TypeConverters({Converters.class})
public abstract  class Database extends RoomDatabase {


    public static final String DATABASE_NAME = "coins_db";
    private static Database instance;


    public static Database getInstance(final Context context){
        if(instance == null){
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    Database.class,
                    DATABASE_NAME
            ).build();
        }
        return instance;
    }
    public abstract CurrencyDao getDao();

}

