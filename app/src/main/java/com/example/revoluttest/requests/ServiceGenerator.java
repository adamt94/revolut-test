package com.example.revoluttest.requests;


import com.example.revoluttest.util.LiveDataCallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl("https://revolut.duckdns.org/")
                    .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create());

    private static  Retrofit retrofit = retrofitBuilder.build();

    private static CurrencyListApi mApi = retrofit.create(CurrencyListApi.class);

    public static CurrencyListApi getApi(){
        return mApi;
    }

}
