package com.example.revoluttest.requests;

import androidx.lifecycle.LiveData;

import com.example.revoluttest.model.CurrencyResponse;
import com.example.revoluttest.requests.ApiResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyListApi {

    // GET  COIN REQUEST
    @GET("/latest")
    LiveData<ApiResponse<CurrencyResponse>> getCurrency(
            @Query("base") String base

    );
}
