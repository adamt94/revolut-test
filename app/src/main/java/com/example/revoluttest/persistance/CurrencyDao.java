package com.example.revoluttest.persistance;


import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.revoluttest.model.Currency;

import java.util.List;
import java.util.Map;

import static androidx.room.OnConflictStrategy.IGNORE;

@androidx.room.Dao
public interface CurrencyDao {

    @Insert(onConflict = IGNORE)
    long [] insertCurrency(Currency... currencies );

    @Update
    int updateCurrency(Currency... currencies);


    @Query("UPDATE currency SET code =:code, rates = :rates where code = :code")
    int updateCurrencyItem(String code, Map<String,Double> rates);

    @Query("SELECT * from currency")
    LiveData<List<Currency>> getCurrency();


}
