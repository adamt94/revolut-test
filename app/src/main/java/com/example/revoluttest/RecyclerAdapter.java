package com.example.revoluttest;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.revoluttest.model.Currency;
import com.example.revoluttest.util.TextWatcherListener;

import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "RecyclerAdapter";


    private List<Currency> mCurrency;

    private double enteredAmount = 0;
    private TextWatcherListener mListener;
    private int focus;


    public RecyclerAdapter(TextWatcherListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items,parent,false);
        return new ViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).currencyCode.setText(mCurrency.get(position).getCode());
        ((ViewHolder) holder).conversion.setText(Double.toString(mCurrency.get(position).getConversionRate()));
        ((ViewHolder) holder).currencyName.setText(mCurrency.get(position).getTitle());
           // RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_launcher_background);
           // Glide.with(holder.itemView.getContext()).setDefaultRequestOptions(requestOptions).load(mCoins.get(position).getImage()).into(((ViewHolder) holder).icon);
    }

    public void setCurrency(List<Currency> currencies){
        mCurrency = currencies;
        focus = -1;
        notifyDataSetChanged();
    }
    public void setCurrency(List<Currency> currencies, int position){


        for(int i = 0; i< currencies.size(); i++){
            if(i != position){
                mCurrency.set(i,currencies.get(i));
                 notifyItemChanged(i);
            }

        }




    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if(mCurrency != null)
            return mCurrency.size();
        return 0;
    }




    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView currencyName,currencyCode;
        ImageView icon;
        EditText conversion;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            currencyCode = itemView.findViewById(R.id.currency_title);
            currencyName = itemView.findViewById(R.id.currency_name);
            icon = itemView.findViewById(R.id.icon);
            conversion = itemView.findViewById(R.id.conversion_rate);
            conversion.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mListener.beforeTextChanged(itemView,charSequence,  i,  i1,  i2);
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mListener.onTextChanged(itemView,charSequence,i,i1,i2,getAdapterPosition());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

    }
}
