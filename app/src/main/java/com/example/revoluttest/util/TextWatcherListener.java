package com.example.revoluttest.util;

import android.text.Editable;
import android.view.View;

public interface TextWatcherListener {
    public void afterTextChanged(View v, Editable s);

    public void onTextChanged(View v, CharSequence s, int start, int before, int count,int position);

    public void beforeTextChanged(View v, CharSequence s, int start, int count, int after);


}
