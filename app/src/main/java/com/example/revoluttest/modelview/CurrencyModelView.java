package com.example.revoluttest.modelview;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.example.revoluttest.model.Currency;
import com.example.revoluttest.repository.Repository;
import com.example.revoluttest.util.Resource;

import java.util.List;

public class CurrencyModelView extends AndroidViewModel {

    private Repository mRepository;
    private MediatorLiveData<Resource<List<Currency>>> mCurrency = new MediatorLiveData<>();

    public CurrencyModelView(@NonNull Application application) {
        super(application);
        mRepository = Repository.getInstance(application);
    }


    public LiveData<Resource<List<Currency>>> getCurrency(){
        return mCurrency;
    }

    public void setCurrencyRates(List<Currency> currencies){
        Repository.updateCurrencies(currencies);

    }

    public void getCurrencyApi(final String base) {
       final  LiveData<Resource<List<Currency>>> respositorySource = Repository.getConversion(base);
        mCurrency.addSource(respositorySource, new Observer<Resource<List<Currency>>>() {
            @Override
            public void onChanged(Resource<List<Currency>> listResource) {
                if(listResource!=null){
                    mCurrency.setValue(listResource);
                }else{
                    mCurrency.removeSource(respositorySource);
                }
            }
        });
        final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                final LiveData<Resource<List<Currency>>> respositorySource2 = Repository.getConversion(base);
                mCurrency.addSource(respositorySource2, new Observer<Resource<List<Currency>>>() {
                    @Override
                    public void onChanged(Resource<List<Currency>> listResource) {
                        if(listResource!=null){
                            mCurrency.setValue(listResource);
                        }else{
                            mCurrency.removeSource(respositorySource2);
                        }
                    }
                });
                ha.postDelayed(this, 10000);
            }
        }, 10000);


        }

    }

